﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb; // references the rigidbody
    public float speed; // sets the speed the player moves
    private bool move; // determines whether or not the player can move
    public int score;
    public Text scoreText;

    void Start() // runs when the application starts, initializes variables
    {
        rb = GetComponent<Rigidbody2D>(); // references the rigidbody component
        move = true; // sets move to true by default
        score = 0;
    }

    void Update() // runs every tick
    {
        if (move) // continues if move is true
        {
            if (Input.GetKey("left shift")) // continues if left shift is pressed
            {
                if (Input.GetKeyDown("d")) // continues if d is pressed
                {
                    transform.Translate(0, -1, 0); // moves the player one unit
                }
                if (Input.GetKeyDown("a")) // continues if a is pressed
                {
                    transform.Translate(0, 1, 0);// moves the player one unit
                }
                if (Input.GetKeyDown("w")) // continues if w is pressed
                {
                    transform.Translate(1, 0, 0);// moves the player one unit
                }
                if (Input.GetKeyDown("s")) // continues if s is pressed
                {
                    transform.Translate(-1, 0, 0);// moves the player one unit
                }
            }
            else // runs if shift is not pressed
            {
                float moveHorizontal = Input.GetAxis("Horizontal"); // gets the user input of a and d
                float moveVertical = Input.GetAxis("Vertical"); // gets the user input of w and s

                Vector3 movement = new Vector3(moveHorizontal, moveVertical, 0.0f); // sets movement to a vector three based on the user input
                rb.velocity = movement * speed; // moves the player
            }

        }

        if (Input.GetKeyDown("p")) // continues if p is pressed
        {
            move = !move; // toggles move
        }

        if (Input.GetKeyDown("q")) // continues if q is pressed
        {
            gameObject.SetActive(false); // disables the player game object
        }

        if (Input.GetKeyDown("escape")) // continues if esc is pressed
        {
            Application.Quit(); // quits the application if its a built game
            //EditorApplication.isPlaying = false; // stops the editor from running the game
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            score++;
            other.gameObject.SetActive(false);
            scoreText.text = "" + score;

        }
    }
}